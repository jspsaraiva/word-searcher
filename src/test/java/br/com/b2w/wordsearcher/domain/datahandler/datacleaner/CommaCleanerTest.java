package br.com.b2w.wordsearcher.domain.datahandler.datacleaner;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommaCleanerTest {

    @Test
    public void mustRemoveNullValues(){
        // Arrange
        String wordWithWrapLina = "bla, bla, bla";

        // Execute
        String wordCleaned = new CommaCleaner().clean(wordWithWrapLina);

        //Assert
        assertEquals("bla bla bla", wordCleaned);
    }

}