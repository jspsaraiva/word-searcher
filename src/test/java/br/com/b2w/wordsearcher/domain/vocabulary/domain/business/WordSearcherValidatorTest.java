package br.com.b2w.wordsearcher.domain.vocabulary.domain.business;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.ArrayList;

import static com.google.common.collect.Lists.newArrayList;

public class WordSearcherValidatorTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void mustThrowExceptionToEmptyList(){
        // Arrange
        ArrayList<?> emptyList = newArrayList();

        //Assert
        exception.expectMessage("Sorry, There is no register for this word!!");
        exception.expect(RuntimeException.class);

        // Execute
        WordSearcherValidator.validateQuantity(emptyList);

    }

}