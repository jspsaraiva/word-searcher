package br.com.b2w.wordsearcher.domain.datahandler.datacleaner;

import org.junit.Test;

import static org.junit.Assert.*;

public class NullValueCleanerTest {

    @Test
    public void mustRemoveNullValues(){
        // Arrange
        String wordWithWrapLina = "bla null bla null bla";

        // Execute
        String wordCleaned = new NullValueCleaner().clean(wordWithWrapLina);

        //Assert
        assertEquals("bla  bla  bla", wordCleaned);
    }

}