package br.com.b2w.wordsearcher.domain.datahandler.datacleaner;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LineWrapCleanerTest {

    @Test
    public void mustRemoveLineWrap(){
        // Arrange
        String wordWithWrapLina = "blablabla\nblabla";

        // Execute
        String wordCleaned = new LineWrapCleaner().clean(wordWithWrapLina);

        //Assert
        assertEquals("blablabla blabla", wordCleaned);
    }

}