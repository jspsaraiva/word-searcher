package br.com.b2w.wordsearcher.exception.domain;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class WordNotFoundException extends RuntimeException {

    public WordNotFoundException(String message) {
       super(message);
    }

}
