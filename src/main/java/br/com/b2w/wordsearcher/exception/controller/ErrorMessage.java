package br.com.b2w.wordsearcher.exception.controller;

import lombok.Getter;

import java.util.List;

@Getter
public class ErrorMessage {

    private List<String> messages;

    public ErrorMessage(List<String> messages) {
        this.messages = messages;
    }

}
