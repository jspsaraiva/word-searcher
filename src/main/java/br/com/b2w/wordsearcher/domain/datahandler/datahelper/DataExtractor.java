package br.com.b2w.wordsearcher.domain.datahandler.datahelper;

import java.util.Map;

import static br.com.b2w.wordsearcher.utility.JsonUtil.getMap;
import static java.util.Objects.requireNonNull;

/**
 *  Data Extractor to get terms of TermVectorsResponse
 *  Unfortunetely, I didnt find any other way to get it
 *
 * **/
public class DataExtractor {

    private static final String FIELD_ANALYSED = "words";
    private static final String TERMS = "terms";
    private static final String TERM_FREQ = "term_freq";

    public static Integer extractHitsNumber(Object termVectors, String word){
        requireNonNull(termVectors);
        requireNonNull(word);

        Map props = getMap(termVectors);
        Map words = getMap(props.get(FIELD_ANALYSED));
        Map statistic = getMap(words.get(TERMS));
        Map wordLooked = getMap(statistic.get(word));

        return (Integer) wordLooked.get(TERM_FREQ);
    }

}
