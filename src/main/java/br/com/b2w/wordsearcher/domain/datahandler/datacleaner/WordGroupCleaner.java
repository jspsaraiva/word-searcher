package br.com.b2w.wordsearcher.domain.datahandler.datacleaner;

import java.util.Objects;

public class WordGroupCleaner {

    public static String clean(String words){
        Objects.requireNonNull(words, "There is a problem here. Word should not be null");

        String wordsWithoutComma = new CommaCleaner().clean(words);
        String wordsWithoutLineWrap = new LineWrapCleaner().clean(wordsWithoutComma);
        String wordsWithoutNullValue = new NullValueCleaner().clean(wordsWithoutLineWrap);

        return wordsWithoutNullValue;

    }

}
