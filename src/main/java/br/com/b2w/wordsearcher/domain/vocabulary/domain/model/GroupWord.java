package br.com.b2w.wordsearcher.domain.vocabulary.domain.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
@Document(indexName = "vocabulary", type = "groupWord")
public class GroupWord {

    @Id
    private String id;

    @Field(type = FieldType.String)
    private String name;

    @Field(type = FieldType.String,
            searchAnalyzer = "standard",
            analyzer = "standard")
    private String words;

}
