package br.com.b2w.wordsearcher.domain.datahandler.datahelper;

import java.util.Map;
import java.util.Objects;

import static br.com.b2w.wordsearcher.utility.JsonUtil.getMap;

public class DataAnalyser {

    public static final String TIME_DURATION_KEY = "took";

    public static int analyseRequestDuration(Object termVectors) {
        Objects.requireNonNull(termVectors);

        Map map = getMap(termVectors);
        return (int) map.get(TIME_DURATION_KEY);
    }
}
