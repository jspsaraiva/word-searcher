package br.com.b2w.wordsearcher.domain.vocabulary.web.view;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@ApiModel(value = "Response", description = "Information about request")
public class WordHitsResponse {

    @ApiModelProperty(value = "Time in Milles", dataType = "long")
    private long timeInMilles;

    @ApiModelProperty(value = "Hits Number", dataType = "int")
    private int numberHits;

}
