package br.com.b2w.wordsearcher.domain.vocabulary.infra.dao;

import br.com.b2w.wordsearcher.domain.vocabulary.domain.model.GroupWord;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface GroupWordRepository extends ElasticsearchRepository<GroupWord, String> {
}
