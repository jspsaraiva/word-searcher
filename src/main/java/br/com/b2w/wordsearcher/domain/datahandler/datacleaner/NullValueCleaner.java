package br.com.b2w.wordsearcher.domain.datahandler.datacleaner;

class NullValueCleaner implements DataCleaner {

    @Override
    public String clean(String word) {
        return word.replaceAll("null", "");
    }

}
