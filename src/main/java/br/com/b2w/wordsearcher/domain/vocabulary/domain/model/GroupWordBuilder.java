package br.com.b2w.wordsearcher.domain.vocabulary.domain.model;

public class GroupWordBuilder {

    private String id;
    private String name;
    private String words;

    public static GroupWordBuilder builder(){
        return new GroupWordBuilder();
    }

    public GroupWordBuilder withID(String id){
        this.id = id;
        return this;
    }

    public GroupWordBuilder withName(String name){
        this.name = name;
        return this;
    }

    public GroupWordBuilder withWords(String words){
        this.words = words;
        return this;
    }

    public GroupWord build(){
        GroupWord groupWord = new GroupWord();
        groupWord.setId(id);
        groupWord.setName(name);
        groupWord.setWords(words);

        return groupWord;
    }


}
