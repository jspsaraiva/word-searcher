package br.com.b2w.wordsearcher.domain.vocabulary.web.controller;

import br.com.b2w.wordsearcher.domain.vocabulary.domain.business.WordSearcherService;
import br.com.b2w.wordsearcher.domain.vocabulary.web.view.WordHitsResponse;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/wordsearcher")
public class WordSearcherController {

    private WordSearcherService wordSearcherService;

    @Autowired
    public WordSearcherController(WordSearcherService wordSearcherService) {
        this.wordSearcherService = wordSearcherService;
    }

    @GetMapping
    @ResponseBody
    @ApiOperation(value = "Search a word in our database vocabulary")
    public WordHitsResponse search(@RequestParam String word){
        return wordSearcherService.findNumberOfHitsFor(word);
    }

}
