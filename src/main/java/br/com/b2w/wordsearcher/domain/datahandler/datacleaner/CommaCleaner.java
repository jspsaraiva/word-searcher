package br.com.b2w.wordsearcher.domain.datahandler.datacleaner;

class CommaCleaner implements DataCleaner {

    @Override
    public String clean(String word) {
        return word.replaceAll(",","");
    }

}
