package br.com.b2w.wordsearcher.domain.vocabulary.infra.dao;

import org.elasticsearch.action.termvectors.TermVectorsResponse;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GroupWordDAO {

    private ElasticsearchOperations elasticsearchTemplate;

    @Autowired
    public GroupWordDAO(ElasticsearchOperations elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    public TermVectorsResponse getHitDetailsInDocument(String idDocument, String word){
        return elasticsearchTemplate.getClient()
                .prepareTermVectors()
                .setIndex("vocabulary")
                .setType("groupWord")
                .setSelectedFields("words")
                .setId(idDocument)
                .get();
    }

    public List<String> getDocumentsThatHasThis(String word) {
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder()
                .withIndices("vocabulary")
                .withTypes("groupWord")
                .withQuery(QueryBuilders.matchQuery("words", word))
                .build();

        return elasticsearchTemplate.queryForIds(searchQuery);
    }
}
