package br.com.b2w.wordsearcher.domain.vocabulary.domain.business;

import br.com.b2w.wordsearcher.domain.datahandler.datahelper.DataProcessor;
import br.com.b2w.wordsearcher.domain.vocabulary.domain.model.DataRequest;
import br.com.b2w.wordsearcher.domain.vocabulary.infra.dao.GroupWordDAO;
import br.com.b2w.wordsearcher.domain.vocabulary.web.view.WordHitsResponse;
import com.google.common.collect.Lists;
import org.elasticsearch.action.termvectors.TermVectorsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WordSearcherService {

    private GroupWordDAO groupWordDAO;

    @Autowired
    public WordSearcherService(GroupWordDAO groupWordDAO) {
        this.groupWordDAO = groupWordDAO;
    }

    public WordHitsResponse findNumberOfHitsFor(String word){
        List<String> idDocuments = groupWordDAO.getDocumentsThatHasThis(word);

        WordSearcherValidator.validateQuantity(idDocuments);

        List<DataRequest> hitsDetails = Lists.newArrayList();

        idDocuments.forEach( id -> {
            DataRequest requestDetail = this.findHitsInDocument(id, word);
            hitsDetails.add(requestDetail);
        });

        return new WordHitsResponse( getTotalTime(hitsDetails), getTotalHits(hitsDetails) );
    }

    public DataRequest findHitsInDocument(String idDocument, String word){
        TermVectorsResponse response = groupWordDAO.getHitDetailsInDocument(idDocument, word);
        return DataProcessor.process(response, word);
    }

    private long getTotalTime(List<DataRequest> dataInfos) {
        return dataInfos.stream()
                    .mapToInt(DataRequest::getTime)
                    .sum();
    }

    private int getTotalHits(List<DataRequest> dataInfos) {
        return dataInfos.stream()
                    .mapToInt(DataRequest::getNumberHits)
                    .sum();
    }

}
