package br.com.b2w.wordsearcher.domain.datahandler.datacleaner;

import org.springframework.stereotype.Component;

@Component
public interface DataCleaner {

    String clean(String word);

}
