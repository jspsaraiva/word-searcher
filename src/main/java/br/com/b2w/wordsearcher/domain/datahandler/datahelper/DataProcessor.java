package br.com.b2w.wordsearcher.domain.datahandler.datahelper;

import br.com.b2w.wordsearcher.domain.vocabulary.domain.model.DataRequest;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.termvectors.TermVectorsResponse;
import org.elasticsearch.common.xcontent.XContentBuilder;

import java.io.IOException;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static org.elasticsearch.common.xcontent.ToXContent.EMPTY_PARAMS;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.elasticsearch.common.xcontent.XContentHelper.createParser;

@Log4j2
public class DataProcessor {

    private static final String TERM_VECTORS_KEY = "term_vectors";

    public static DataRequest process(TermVectorsResponse response, String word){
        requireNonNull(response);
        requireNonNull(word);

        DataRequest dataRequest = new DataRequest();

        try {
            XContentBuilder builder = jsonBuilder().startObject();
            response.toXContent(builder, EMPTY_PARAMS);
            builder.endObject();

            Map<String, Object> termsMap = createParser(builder.bytes()).map();

            dataRequest.setNumberHits( DataExtractor.extractHitsNumber(termsMap.get(TERM_VECTORS_KEY), word) );
            dataRequest.setTime( DataAnalyser.analyseRequestDuration(termsMap));

        } catch (IOException e) {
            log.error("Error to process request detail: ", e.getMessage());
            throw new RuntimeException("Sorry, A error occurred to process request detail", e);
        }

        return dataRequest;
    }
}
