package br.com.b2w.wordsearcher.domain.vocabulary.domain.model;

import lombok.Data;

@Data
public class DataRequest {

    private int time;
    private int numberHits;

}
