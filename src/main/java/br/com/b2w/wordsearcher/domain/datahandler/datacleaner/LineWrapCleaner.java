package br.com.b2w.wordsearcher.domain.datahandler.datacleaner;

class LineWrapCleaner implements DataCleaner {

    public String clean(String word) {
        return word.replaceAll("\n", " ");
    }

}
