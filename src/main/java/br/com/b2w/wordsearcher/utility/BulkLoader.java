package br.com.b2w.wordsearcher.utility;

import br.com.b2w.wordsearcher.domain.datahandler.datacleaner.WordGroupCleaner;
import br.com.b2w.wordsearcher.domain.vocabulary.domain.model.GroupWord;
import br.com.b2w.wordsearcher.domain.vocabulary.domain.model.GroupWordBuilder;
import br.com.b2w.wordsearcher.domain.vocabulary.infra.dao.GroupWordRepository;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static br.com.b2w.wordsearcher.utility.DirectoryEnum.DEFAULT_DIRECTORY;

@Service
@Log4j2
public class BulkLoader {

    private LoaderFile loaderFile;
    private GroupWordRepository repository;

    @Autowired
    public BulkLoader(LoaderFile loaderFile, GroupWordRepository repository) {
        this.loaderFile = loaderFile;
        this.repository = repository;
    }

    public void load() throws IOException, URISyntaxException {
        File[] files = getFilesToLoad();

        List<GroupWord> groupWords = Lists.newArrayList();

        for (File file : files){
            String words = loaderFile.load(file.getName());
            String wordsCleaned = WordGroupCleaner.clean(words);

            GroupWord groupWord = GroupWordBuilder.builder()
                                    .withName(file.getName())
                                    .withWords(wordsCleaned)
                                    .build();

            groupWords.add(groupWord);
        }

        repository.save(groupWords);
    }

    private File[] getFilesToLoad() throws IOException {
        return new File(DEFAULT_DIRECTORY.getPath()).listFiles();
    }

}
