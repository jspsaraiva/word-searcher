package br.com.b2w.wordsearcher.utility;

public enum DirectoryEnum {

    DEFAULT_DIRECTORY("/tmp/volume/");

    private String path;

    DirectoryEnum(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
