package br.com.b2w.wordsearcher.utility;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.stream.Stream;

import static br.com.b2w.wordsearcher.utility.DirectoryEnum.DEFAULT_DIRECTORY;
import static java.text.MessageFormat.*;
import static java.text.MessageFormat.format;

@Component
@Log4j2
public class LoaderFile {

    private static final String ISO_8859_1 = "ISO-8859-1";
    private static final String UTF_8 = "UTF-8";

    /**
     * There is 2 ways to load file. If was not possible load it with default encoding (UTF-8),
     * it will be load in ISO 8859-1 encoding
     **/
    public String load(String filename) throws IOException, URISyntaxException {
        String data = null;

        try {
            data = readFile(filename, UTF_8);
        } catch (UncheckedIOException e){
            data = readFile(filename, ISO_8859_1);
        } catch (Exception e){
            log.error("Sorry, has occurred some issue to load info to database!!");
        }

        return data;
    }

    private String readFile(String filename, String encoding) throws URISyntaxException, IOException {
        String fileWithPath = format(DEFAULT_DIRECTORY.getPath() + filename);

        Path path = Paths.get(new File(fileWithPath).toURI());

        StringBuilder data = new StringBuilder();
        Stream<String> lines = Files.lines(path, Charset.forName(encoding));
        lines.forEach(line -> data.append(line).append("\n"));
        lines.close();

        return data.toString();
    }
}