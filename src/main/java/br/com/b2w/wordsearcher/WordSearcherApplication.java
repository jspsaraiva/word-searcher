package br.com.b2w.wordsearcher;

import br.com.b2w.wordsearcher.utility.BulkLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WordSearcherApplication implements CommandLineRunner {

	@Autowired
	private BulkLoader bulkLoader;

	public static void main(String[] args) {
		SpringApplication.run(WordSearcherApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		bulkLoader.load();
	}

}