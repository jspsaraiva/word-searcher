# Word-searcher App

### Objetivo

- Aplicaçâo criada para disponibilizar atraves de uma API Rest, consulta a um
vocabulario local.

###  Requisitos para rodar a aplicação:
    - Docker  
    - Docker compose  
  
###  Passo a Passo
      1º - Clonar o projeto para a maquina local  
      2º - Abrir o terminal na pasta do projeto  
      3º - Executar o comando "mvn clean package"    
      4º - Executar o comando "docker build -t "word-searcher:1.0" . " 
      5º - Executar o comando "docker-compose up"  

###  Acesso a aplicação
    A API esta disponivel atraves da URL:
      URL: http://localhost:8080/api/wordsearcher
    Exemplo de Consulta:
      URL: GET http://localhost:8080/api/wordsearcher?word=abacaxi
      
## Arquitetura

A arquitetura prosposta para a implementação deste projeto foi adotar:

- *SpringBoot*: Um framework web sobe convençâo, que possui uma plataforma robusta para desenvolvimento de APIs. Possui starters que contribuiram para dar velocidade durante o desenvolvimento. Um deles o Spring Data ElasticSearch para interaçâo com o DB escolhido.

- *ElasticSearch*: O ES é um banco NoSQL, Open Source e extremamente performatico para buscas full-text search, por trabalhar com indexação invertida. Eu o considerei uma escolha viavel por que alem de atender performaticamente, ele e um banco escalavel horizontalmente o que nos permite ter tolerancia a falha e replicacao de dados. 

- *Docker*: Utilizei docker para empacotar as aplicacoes e no caso da aplicacao SpringBoot uma vez que a mesma segue o arquitetura Stateless, o container poderia ser replicado dezena de vezes e apenas receber request distribuidas por um
load balance, atendendo ao requisito de ser uma aplicacao escalavel.
  
Para este projeto eu disponibilizei o ES em um container docker porem em um cenario real eu optaria 
por armazena-lo e utiliza-lo atraves de um servico cloud como o Amazon ES, assim adquirindo facilidade 
de escalar, controlar redundancia, atualizar versoes e controlar a seguranca do banco.

Para build, execução dos testes e deploy eu utilizaria o Jenkins.

A documentação da API pode ser encontrada em -> http://localhost:8080/swagger-ui.html   
E o ES consultado diretamente pela URL -> http://localhost:9200/  

### Considerações

- Assumi que para uma palavra ser igual e incrementar um **hit** na contagem, a acentuaçâo precisa estar correta.
- Adicionei um skip no maven para que ao gerar a imagem da aplicacao nao ocorra erro (por nao encontrar o container do ES). O cenario ideal seria configurar 
um profile para que essa conexao fosse testada somente com o profile container.
