FROM frolvlad/alpine-oraclejdk8:slim
ADD target/word-searcher-0.0.1-SNAPSHOT.jar //
EXPOSE 8080
ADD volume /tmp/volume/
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=container", "/word-searcher-0.0.1-SNAPSHOT.jar"]
